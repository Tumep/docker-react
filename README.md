# Docker TICK-stack with python scripts

This creates a simple tick-stack with all the required bells and whistles for monitoring influxdb and weather + citybike outputs. Python scripts are in the scripts directory and consumed from:

| Source | URL | LICENSE |
| ------ | ------ | ------ |
| Finnish Meteorological Institute (FMI) | https://en.ilmatieteenlaitos.fi/open-data | https://creativecommons.org/licenses/by/4.0/ |
| Helsinki Region Public Transport (HSL) | https://www.hsl.fi/en/opendata | https://creativecommons.org/licenses/by/4.0/ |

[[_TOC_]]

### TODO
- Add ruuvi scripts for additional indoor monitoring
- Add kapacitor (K) alarms (telegram?)

### DONE
- Create a working TIC stack
- Create and integrate python scripts to monitor FMI and HSL
- Change from ubuntu images to Alpine where possible (Smaller footprint)

## How to use

### Starting 
1. Build the customized docker image using the Dockerfile when in the folder and tag it with ```telegraf-alpine-python3``` tag

```
docker build -t telegraf-alpine-python3:latest .
```

2. Check the configuration files that the inputs and outputs point to right places and the docker-compose file for volume names

3. Create volumes for influxdb and chronograf for persistance

```
docker volume create chronograf-volume 
docker volume create influxdb-volume
```

4. Run the docker-compose, network is created automatically

```
docker-compose up -d
```

### Stopping and deleting data

#### Stop
```
docker-compose down
```
#### Delete data
```
docker volume delete chronograf-volume 
docker volume delete influxdb-volume
```